import React from 'react';
import {Modal, ModalBody, ModalClose, ModalFooter, ModalHeader, ModalWrapper} from "./Modal"
import "./ModalImage.scss"


const ModalImage = ({img, content, onClose, alt, firstText, secondaryText, buttonOneTitle, buttonTwoTitle, secondaryClick}) => {
    return (
        <ModalWrapper onClickOutside={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    {img !== undefined ? <img className="modal-img" src={img} alt={alt} /> : null}
                    <p>{content}</p>
                </ModalBody>
                <ModalFooter firstClick={onClose} secondaryClick={secondaryClick} firstText={firstText} secondaryText={secondaryText} buttonOneTitle={buttonOneTitle} buttonTwoTitle={buttonTwoTitle}>

                </ModalFooter>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalImage;