import { configureStore } from '@reduxjs/toolkit';
import modalWindowReducer from './modalWIndow.slice'
import productsReducer from './products.slice'
import favouritesReducer from './favourites.slice'
import cartReducer from './cart.slice'
import deleteModalWindowReducer from './deleteModalWindow.slice';

export const store = configureStore({
  reducer: {
    modalWindow: modalWindowReducer,
    deleteModal: deleteModalWindowReducer,
    products: productsReducer,
    favourites: favouritesReducer,
    cart: cartReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware()
})