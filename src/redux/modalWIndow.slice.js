import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isOpened: false, 
    product: null
}

const modalWindowSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        openModalWindow: (state, action) => {
            state.isOpened = true;
            state.product = action.payload;
        },
        closeModalWindow: (state, action) => {
            state.isOpened = false;
            state.product = null;
        }
    }
})

export const {openModalWindow, closeModalWindow} = modalWindowSlice.actions;

export default modalWindowSlice.reducer;