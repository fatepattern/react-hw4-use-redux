import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isOpened: false, 
    product: null
}

const deleteModalWindowSlice = createSlice({
    name: "delete-modal",
    initialState,
    reducers: {
        openDeleteModalWindow: (state, action) => {
            state.isOpened = true;
            state.product = action.payload;
        },
        closeDeleteModalWindow: (state, action) => {
            state.isOpened = false;
            state.product = null;
        }
    }
})

export const {openDeleteModalWindow, closeDeleteModalWindow} = deleteModalWindowSlice.actions;

export default deleteModalWindowSlice.reducer;