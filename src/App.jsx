import './App.css'
import Header from './components/Header/Header';
import { useEffect, useState } from 'react'
import { Routes, Route, NavLink } from 'react-router-dom';
import Favourites from './Pages/Favourites';
import Home from './Pages/Home';
import Cart from './Pages/Cart';
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from './redux/products.slice';
import { updateFavourites } from './redux/favourites.slice';
import { updateCart } from './redux/cart.slice';



function App() {

  useEffect(() => {
    dispatch(fetchProducts())
  }, [])


  const dispatch = useDispatch();
  const productsData = useSelector(state => state.products.products);
  const favourites = useSelector(state => state.favourites.favourites);
  const cart = useSelector(state => state.cart.cart);

  // const [cart, addToCart] = useState([]);

  

  return (
    <>

      <Header>
      </Header>

      <Routes>
        <Route path='/' element={<Home products={productsData} favouritesList={favourites} setFavouritesList={(product) => dispatch(updateFavourites(product))} cart={cart} addToCart={(product) => dispatch(updateCart(product))}/>}></Route>
        <Route path='/favourites' element={<Favourites cart={cart} addToCart={(product) => dispatch(updateCart(product))} favouritesList={favourites} setFavouritesList = {(product) => dispatch(updateFavourites(product))}/>}></Route>
        <Route path='/cart' element={<Cart cart={cart} addToCart={(product) => dispatch(updateCart(product))} products={productsData} favouritesList={favourites} setFavouritesList={(product) => dispatch(updateFavourites(product))}/>}></Route>
      </Routes>

      

      

    </>
  )
}

export default App