import { useState } from "react";
import ProductsCard from "../components/Products/ProductsCard";
import ModalImage from "../components/Modal/ModalImage";
import { useDispatch, useSelector } from "react-redux";
import { updateCart } from "../redux/cart.slice";
import { closeDeleteModalWindow, openDeleteModalWindow } from "../redux/deleteModalWindow.slice";

export default function Cart({addToCart, products, favouritesList, setFavouritesList}){

    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.cart);

    let deleteButtons = document.querySelectorAll(".add-to-cart-button");

    const isOpened = useSelector(state => state.deleteModal.isOpened);
    const productToDelete = useSelector(state => state.deleteModal.product);
    

    deleteButtons.forEach(button => {
        button.style.display = "none";
    })

    const handleDeleteButtonClick = (product) => {
        dispatch(openDeleteModalWindow(product))
    }

    const handleConfirmDelete = () => {
        const newCart = cart.filter(item => item.id !== productToDelete.id);
        dispatch(updateCart(newCart))
        localStorage.setItem('cartItems', JSON.stringify(newCart));
        dispatch(closeDeleteModalWindow())
    }

    console.log(cart);

    return (
        <div className="cart-list">

            {isOpened && (
                <ModalImage
                onClose={() => dispatch(closeDeleteModalWindow())}
                firstText="Are you sure you want to remove this item from your cart?"
                secondaryText="Remove item"
                buttonOneTitle="Cancel"
                buttonTwoTitle="Remove"
                secondaryClick={handleConfirmDelete}
                />
            )}

            {cart.map(item => (

                <ProductsCard 
                    key={item.id} 
                    product={item}
                    products={products}
                    cart={cart} 
                    addToCart={(product) => dispatch(updateCart(product))}
                    favouritesList={favouritesList} 
                    setFavouritesList={setFavouritesList}
                >

                    <div className="cart-list__delete-btn" onClick={() => handleDeleteButtonClick(item)}>
                        <img src="../../public/118584_x_icon.svg" alt="x-icon" />
                    </div>

                </ProductsCard>
            ))}
        </div>
    )
}